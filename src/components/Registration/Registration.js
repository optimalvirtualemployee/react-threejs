import React, { Component } from 'react';

class Registration extends Component {
    constructor(props) {
        super(props)
        this.state = props.registrationData
    }

    render() {
        return (
            <section>
                <div className="container">
                    <div className="row mt-5 mb-5">
                        <div className="col-lg-6 border p-4">
                            <div className="inner contact-right">
                                <h4 className="mb-2">Create an Account!</h4>
                                <form action="#" method="post" className="signin-form">
                                    <div className="input-grids">
                                        <div className="form-group">
                                            <input type="text" name="name" id="name" placeholder="Your Name*" className="contact-input" />
                                        </div>
                                        <div className="form-group">
                                            <input type="email" name="sender" id="sender" placeholder="Your Email*" className="contact-input" required />
                                        </div>
                                        <div className="form-group d-flex">
                                            <input type="text" name="company" id="company" placeholder="Company*" className="contact-input w-50 mr-2" required />
                                            <input type="tel" name="phone" id="phone" placeholder="Phone*" className="contact-input w-50" />
                                        </div>
                                    </div>
                                    <div className="form-group">
                                        <input type="text" name="address" id="address" placeholder="Street" className="contact-input" />
                                    </div>
                                    <div className="form-group">
                                        <input type="text" name="city" id="city" placeholder="City" className="contact-input" />
                                    </div>
                                    <div className="form-group d-flex">
                                        <input type="password" name="password" id="password" placeholder="Password" className="contact-input mr-2" />
                                        <input type="re-password" name="re-password" id="rePassword" placeholder="Re enter your password" className="contact-input" />
                                    </div>
                                    <div className="form-group">
                                        <label htmlFor="terms-conditions" className="terms-conditions">
                                            <input type="checkbox" name="terms-conditions" id="terms-conditions" className="contact-input" />
                          Accept terms &amp; conditions
                        </label>
                                    </div>
                                    <div className="text-center">
                                        <button className="btn btn-primary theme-button">Register Account</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                        <div className="col-lg-6">
                            <div className="inner ">
                                <img src="src/assets/images/signup-bg.png" alt="" className="w-100" />
                                <hr />
                                <a href="#" className="btn-google btn-user btn-block">
                                    <i className="fa fa-google fa-fw" /> Register with Google
                    </a>
                                <a href="#" className="btn-facebook btn-user btn-block">
                                    <i className="fa fa-facebook-f fa-fw" />
                      Register with Facebook
                    </a>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        )
    }
}

export default Registration