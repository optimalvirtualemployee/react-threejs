import React, { Component } from 'react'

class Footer extends Component {
    constructor(props) {
        super(props)
        this.state = props.footerData
    }

    render() {
        return (
            <footer className="w3l-footer-29-main" id="footer">
                <div className="footer-29 py-5">
                    <div className="container pb-lg-3">
                        <div className="row footer-top-29">
                            <div className="col-lg-6 col-md-6 footer-list-29 footer-1 mt-md-4">
                                <h1 className="footer-logo"><a className="footer-logo mb-md-3 mb-2" href="#url">Dwight</a></h1>
                                <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Iste mollitia deserunt repudiandae, ut minusassumenda architecto illum eaqueodio temporibus.</p>
                            </div>
                            <div className="col-lg-3 col-md-6 footer-list-29 footer-2 mt-5">
                                <h6 className="footer-title-29">Explore More</h6>
                                <ul>
                                    <li><a href="#">Gallery</a></li>
                                    <li><a href="#">Blog</a></li>
                                    <li><a href="#">Landing Page</a></li>
                                    <li><a href="#">Privacy Policy</a></li>
                                    <li><a href="#">Terms and conditions</a></li>
                                </ul>
                            </div>
                            <div className="col-lg-3 col-md-6 footer-list-29 footer-4 mt-5">
                                <h6 className="footer-title-29">Quick Links</h6>
                                <ul>
                                    <li><a href="index.html">Home</a></li>
                                    <li><a href="about.html">About</a></li>
                                    <li><a href="services.html">Services</a></li>
                                    <li><a href="#blog.html"> Blog</a></li>
                                    <li><a href="contact.html">Contact</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
                <div id="footers14-block" className="py-3">
                    <div className="container">
                        <div className="footers14-bottom text-center">
                            <div className="social">
                                <a href="#facebook" className="facebook"><span className="fa fa-facebook" aria-hidden="true" /></a>
                                <a href="#google" className="google"><span className="fa fa-google-plus" aria-hidden="true" /></a>
                                <a href="#twitter" className="twitter"><span className="fa fa-twitter" aria-hidden="true" /></a>
                                <a href="#instagram" className="instagram"><span className="fa fa-instagram" aria-hidden="true" /></a>
                                <a href="#youtube" className="youtube"><span className="fa fa-youtube" aria-hidden="true" /></a>
                            </div>
                            <div className="copyright mt-1">
                                <p>© 2020 Dwight. All Rights Reserved</p>
                            </div>
                        </div>
                    </div>
                </div>
            </footer>
        )
    }
}

export default Footer