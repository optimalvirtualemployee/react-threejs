import React, { Component, Fragment } from 'react';

class SubSlider extends Component {
    constructor(props) {
        super(props)

        this.state = {
            slides: props.subsliderData
        }
    }

    render() {
        return (
            <section className="w3l-index2">
                <div className="row abouthy-img-grids no-gutters">
                    {
                        this.state.slides.map((slide, index) => (
                            <Fragment key={index}>
                                <div className="col-lg-4 col-md-6 img-one">
                                    <img src={slide.img} alt=" " className="img-fluid" />
                                </div>
                                <div className="col-lg-4 col-md-6 img-one content-mid py-md-0 py-5">
                                    <div className="info">
                                        <h3>{slide.head}</h3>
                                        <p className="mt-3 mb-5 white">{slide.paragraph}</p>
                                        <a href="about.html" className="btn btn-light theme-button">{slide.buttonText}</a>
                                    </div>
                                </div>
                            </Fragment>
                        ))
                    }
                    {/* <div className="col-lg-4 col-md-6 img-one">
                        <img src="assets/images/img2.jpg" alt=" " className="img-fluid" />
                    </div>
                    <div className="col-lg-4 col-md-6 img-info content-mid py-md-0 py-5">
                        <div className="info">
                            <h3>Heading goes here</h3>
                            <p className="mt-3 mb-5">Lorem ipsum dolor sit amet consectetur adipisicing elit. Temporibus voluptatum, nostrumsed alias accusamus earum.</p>
                            <a href="services.html" className="btn btn-primary theme-button">Our Services</a>
                        </div>
                    </div>

                    <div className="col-lg-4 col-md-6 img-one">
                        <img src="assets/images/img8.jpg" alt=" " className="img-fluid" />
                    </div>
                    <div className="col-lg-4 col-md-6 img-info content-mid py-md-0 py-5">
                        <div className="info">
                            <h3>Heading goes here</h3>
                            <p className="mt-3 mb-5">Lorem ipsum dolor sit amet consectetur adipisicing elit. Temporibus voluptatum, nostrumsed alias accusamus earum.</p>
                            <a href="contact.html" className="btn btn-primary theme-button">Contact Us</a>
                        </div>
                    </div> */}

                </div>
            </section>
        )
    }
}

export default SubSlider