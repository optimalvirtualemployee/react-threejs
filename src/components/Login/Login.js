import React, { Component } from 'react';

class Login extends Component {
    constructor(props) {
        super(props)
        this.state = props.loginData
    }

    render() {
        return (
            <section>
                <div className="container">
                    <div className="row mt-5 mb-5">
                        <div className="col-lg-6 border p-4">
                            <div className="inner contact-right">
                                <h4 className="mb-3">Login Account!</h4>
                                <form action="#" method="post" className="signin-form">
                                    <div className="input-grids">
                                        <div className="form-group">
                                            <input type="email" name="sender" id="sender" placeholder="Your Email*" className="contact-input" required />
                                        </div>
                                    </div>
                                    <div className="form-group d-flex">
                                        <input type="password" name="password" id="password" placeholder="Password" className="contact-input" />
                                    </div>
                                    <div className="form-group d-flex align-items-center justify-content-between">
                                        <label htmlFor="keepMeSignedIn" className="keepMeSignedIn">
                                            <input type="checkbox" name="keepMeSignedIn" id="keepMeSignedIn" className="contact-input" />
                          Keep me Signed In
                        </label>
                                        <a href="registration.html">Register Account</a>
                                    </div>
                                    <div className="text-center">
                                        <button className="btn btn-primary theme-button">Login Now</button>
                                    </div>
                                    <hr />
                                    <a href="#" className="btn-google btn-user btn-block">
                                        <i className="fa fa-google fa-fw" /> Login with Google
                      </a>
                                    <a href="#" className="btn-facebook btn-user btn-block">
                                        <i className="fa fa-facebook-f fa-fw" /> Login with Facebook
                      </a>
                                </form>
                            </div>
                        </div>
                        <div className="col-lg-6 d-lg-block d-sm-none">
                            <div className="inner text-center">
                                <img src="src/assets/images/forget-bg.png" alt="" className="w-75" />
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        )
    }
}

export default Login