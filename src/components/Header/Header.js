import React, { Component } from 'react';
import Select from '@material-ui/core/Select'
import { Link } from 'react-router-dom';

class Header extends Component {
    constructor(props) {
        super(props)

        this.state = {
            country: "none"
        }

    }


    handleChange(event) {
        this.setState({ country: event.target.value });
    }

    render() {
        return (
            <header className="w3l-top-header-strip">
                <div className="hero-header-11">
                    <div className="hero-header-11-content">
                        <div className="main-header">
                            <div className="container-fluid">
                                <div className="d-grid grid-columns-auto">
                                    <div className="right-grid align-left d-grid">
                                        <div className="topStrip">
                                            <div className="selectCountry">
                                                <Select
                                                    native
                                                    value={this.state.country}
                                                    onChange={(e) => this.handleChange(e)}
                                                    inputProps={{
                                                        name: "country",
                                                        id: "country",
                                                        className: "p-1 border pl-3 pr-3"
                                                    }}
                                                    style={{ background: 'white' }}
                                                >
                                                    <option value="none">Select Country</option>
                                                    <option value="In">India</option>
                                                    <option value="Aus">Australia</option>
                                                    <option value="USA">USA</option>
                                                </Select>
                                            </div>
                                            <div className="brand">
                                                <a className="navbar-brand" href="index.html">Dwight Logo</a>
                                            </div>
                                            <div className="rightContent d-flex">
                                                <div className="myAccount mr-3">
                                                    <Link to="/Login">Sign In</Link> |
                                                    <Link to="/Registration">Register</Link> |
                                                </div>
                                                <div className="addToCart">
                                                    <i className="fa fa-shopping-cart text-white" />
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </header>
        )
    }
}

export default Header