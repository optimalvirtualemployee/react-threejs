import React, { Component, Fragment } from 'react'


class Partner extends Component {
    constructor(props) {
        super(props)

        this.state = props.partnerData
    }

    static getDerivedStateFromProps(props, state) {
        console.log(state)
        return {

        }
    }

    componentDidUpdate() {
        console.log("service compDidUpdate")
    }

    render() {
        return (
            <Fragment>
                <section className="w3l-logos py-5">
                    <div className="container py-lg-3">
                        <div className="header-section text-center">
                            <h3>{this.state.head}</h3>
                            <p className="mt-3 mb-5">{this.state.paragraph}</p>
                        </div>
                        <div className="row">
                            {
                                this.state.images.map((path, index) => (
                                    <div key={index} className="col-lg-3 col-6 logo-view">
                                        <img src={path} alt="company-logo" className="img-fluid" />
                                    </div>
                                ))
                            }
                        </div>
                    </div>
                </section>
            </Fragment>
        )
    }
}

export default Partner