import React, { Component, Fragment } from 'react'

class About extends Component {
    constructor(props) {
        super(props)

        this.state = props.aboutData
    }

    static getDerivedStateFromProps(props, state) {
        console.log(state)
        return {

        }
    }

    componentDidUpdate() {
        console.log("about compDidUpdate")
    }

    render() {
        return (
            <Fragment>
                <div className="w3l-index3 pt-5 pb-5" id="about">
                    <div className="container pt-lg-3">
                        <div className="header-section text-center">
                            <h3>{this.state.head}</h3>
                            <p className="mt-3 mb-5">{this.state.paragraph}</p>
                        </div>
                        <div className="aboutgrids row">
                            <div className="col-lg-6 aboutgrid1">
                                <h4>{this.state.subHeading}</h4>
                                <p>{this.state.subParagraph}</p>
                                <ul className="services-list mb-5">
                                    {
                                        this.state.bulletPoints.map((point, index) => (
                                            <li key={index}>{point}</li>
                                        ))
                                    }

                                </ul>
                                <a href="#" className="btn btn-primary theme-button">More services</a>
                            </div>
                            <div className="col-lg-6 aboutgrid2 mt-lg-0 mt-5">
                                <img src="src/assets/images/about.jpg" alt="about image" className="img-fluid" />
                            </div>
                        </div>
                    </div>
                </div>
            </Fragment>
        )
    }
}

export default About