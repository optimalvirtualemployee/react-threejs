import React, { Component, Fragment } from "react";
import OwlCarousel from 'react-owl-carousel2';
// import 'react-owl-carousel2/style.css';

// import 'owl.carousel/dist/assets/owl.theme.default.css'
// import 'owl.carousel/dist/assets/owl.carousel.css';
// import 'owl.carousel'

class MainSlider extends Component {
    constructor(props) {
        super(props)

        this.state = {
            slides: props.slidesData
        }
    }

    static getDerivedStateFromProps(props, state) {
        console.log(state)
        return {

        }
    }

    componentDidMount() {
        console.log(" Main slides compdidmount")
    }

    shouldComponentUpdate(nextProps, nextState) {
        console.log("shouldcompUpdate")
    }


    render() {
        const acoptions = {
            loop: false,
            margin: 0,
            nav: false,
            responsiveClass: "true",
            autoplay: true,
            autoplayTimeout: 5000,
            autoplaySpeed: 1000,
            autoplayHoverPause: false,
            responsive: {
                0: {
                    items: 1,
                    nav: false
                },
                480: {
                    items: 1,
                    nav: false
                },
                667: {
                    items: 1,
                    nav: true
                },
                1000: {
                    items: 1,
                    nav: true
                }
            }
        }

        return (
            <Fragment>
                <section className="w3l-main-slider">
                    <div className="companies20-content">
                        <div className="companies-wrapper">
                            <OwlCarousel className="owl-one owl-carousel owl-theme text-center" options={acoptions}  >
                                {
                                    this.state.slides.map((slide, index) => (
                                        <div key={index} className="item">
                                            <li>
                                                <div className={`slider-info banner-view banner-top${index}`}>
                                                    <div className="banner-info container">
                                                        <p className="slideone">{slide.head}</p>
                                                        <h3 className="banner-text mt-lg-5 mt-md-4 mt-3">{slide.paragraph}</h3>
                                                        <a href="about.html" className="btn btn-primary theme-button">{slide.buttonText}</a>
                                                    </div>
                                                </div>
                                            </li>
                                        </div>
                                    ))
                                }
                            </OwlCarousel>
                        </div>
                    </div>
                </section>
            </Fragment>
        )
    }
}

export default MainSlider