import React, { Component } from 'react';

class Subscribe extends Component {
    constructor(props) {
        super(props)

        this.state = props.subscribeData
    }

    render() {
        return (
            <section className="w3l-subscribe">
                <div className="main-w3 py-5">
                    <div className="container py-lg-3">
                        <div className="grids-forms text-center">
                            <div className="row">
                                <div className="col-md-12">
                                    <span className="fa fa-envelope mb-3" aria-hidden="true" />
                                    <div className="header-section white">
                                        <h3>{this.state.head}</h3>
                                        <p>{this.state.paragraph}</p>
                                    </div>
                                </div>
                            </div>
                            <div className="row mt-5">
                                <div className="col-md-7 mx-auto main-midd-2">
                                    <form action="#" method="post" className="rightside-form">
                                        <input type="email" name="email" placeholder="Your Email Address.." className="mb-0" required />
                                        <button type="submit" className="btn btn-primary theme-button">Subscribe</button>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        )
    }
}

export default Subscribe