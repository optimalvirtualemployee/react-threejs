import React from 'react';
import { Link } from 'react-router-dom';

// import './NavBar.css';

const NavBar = () => {
    return (
        <header className="w3l-header">
            <div className="hero-header-11">
                <div className="hero-header-11-content">
                    <div className="container-fluid">
                        <nav className="navbar navbar-expand-lg navbar-light py-0 px-0">
                            <button className="navbar-toggler collapsed" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                                <span className="navbar-toggler-icon fa icon-expand fa-bars" />
                                <span className="navbar-toggler-icon fa icon-close fa-times" />
                            </button>
                            <div className="collapse navbar-collapse" id="navbarSupportedContent">
                                <ul className="navbar-nav mx-auto">
                                    <li className="nav-item active">
                                        <Link className="nav-link" to="/Home">Home <span className="sr-only">(current)</span></Link>
                                    </li>
                                    <li className="nav-item dropdown">
                                        <Link className="nav-link dropdown-toggle" to="/FabricationService" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                            Fabrication Service
                                        </Link>
                                        <div className="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                                            <Link className="dropdown-item" to="/FabricationService">Service 1</Link>
                                            <Link className="dropdown-item" to="/FabricationService">Service 2</Link>
                                            <Link className="dropdown-item" to="/FabricationService">Service 3</Link>
                                        </div>
                                    </li>
                                    <li className="nav-item @@services-active">
                                        <Link className="nav-link" to="/Product">Products</Link>
                                    </li>
                                    <li className="nav-item @@services-active">
                                        <Link className="nav-link" to="/DesignService">Design Service</Link>
                                    </li>
                                    <li className="nav-item @@contact-active">
                                        <Link className="nav-link" to="/Contact">Contact US</Link>
                                    </li>
                                </ul>
                            </div>
                        </nav>
                    </div>
                </div>
            </div>
        </header>

        ////////////////////////////////////////////////////////////
        // <div className="collapse navbar-collapse" id="navbarSupportedContent">
        //     <ul className="navbar-nav mx-auto">
        //         <li className="nav-item active"><Link to="/Home">Home</Link></li>
        //         <li className="nav-item @@services-active"><Link className="nav-link" to="/FabricationService">FabricationService</Link></li>
        //         <li className="nav-item @@services-active"><Link to="/Product">Products</Link></li>
        //         <li><Link to="/DesignService">DesignService</Link></li>
        //         <li><Link to="/Contact">Contact</Link></li>
        //     </ul>
        //     <hr />
        // </div>
    );
};

export default NavBar;