import React, { Component, Fragment } from 'react'


class Service extends Component {
    constructor(props) {
        super(props)

        this.state = props.serviceData
    }

    static getDerivedStateFromProps(props, state) {
        console.log(state)
        return {

        }
    }

    componentDidUpdate() {
        console.log("service compDidUpdate")
    }

    render() {
        return (
            <Fragment>
                <section className="w3l-index1" id="services">
                    <div className="features-with-17_sur py-5">
                        <div className="container py-lg-3">
                            <div className="header-section text-center">
                                <h3>{this.state.head}</h3>
                                <p className="mt-3 mb-5">{this.state.paragraph}</p>
                            </div>
                            <div className="features-with-17-top_sur">
                                <div className="row">
                                    {
                                        this.state.services.map((service, index) => (
                                            <div key={index} className="col-lg-4 col-md-6 m-top">
                                                <div className="features-with-17-right-tp_sur">
                                                    <div className="features-with-17-left1">
                                                        <span className={`fa ${service.icon}`} aria-hidden="true" />
                                                    </div>
                                                    <div className="features-with-17-left2">
                                                        <h6><a href="#">{service.head}</a></h6>
                                                        <p>{service.paragraph}</p>
                                                    </div>
                                                </div>
                                            </div>
                                        ))
                                    }
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
            </Fragment>
        )
    }
}

export default Service

