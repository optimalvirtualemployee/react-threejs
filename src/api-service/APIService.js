

class APIService {

    // constructor() {

    // }


    ajaxRequest(method, url, args, contentType, callback, callbackOnError, notJSON, authToken) {
        var a, aa, qsp,
            m = method.toLowerCase(),
            u = url,
            xhttp = new XMLHttpRequest();
        qsp = null;

        if (args) {
            if (m !== "get" && contentType === "application/json") {
                qsp = JSON.stringify(args);
            } else {
                aa = [];
                for (a in args) {
                    if (args.hasOwnProperty(a)) {
                        aa.push(encodeURIComponent(a) + "=" + encodeURIComponent(args[a]));
                    }
                }
                qsp = aa.join("&");
            }
        }

        if (m === "get" && qsp) {
            if (u.indexOf("?") === -1) {
                u += "?" + qsp;
            } else {
                u += "&" + qsp;
            }
        }

        xhttp.onreadystatechange = function () {
            if (this.readyState === 4) {
                if (this.status === 200) {
                    if (callback) {
                        if (notJSON) {
                            callback(xhttp.responseText);

                        } else {
                            callback(JSON.parse(xhttp.responseText));
                        }
                    }
                } else {
                    if (callbackOnError) {
                        callbackOnError();
                    }
                }
            }
        };
        xhttp.open(m, u, true);
        if (m !== "get" && contentType !== null) {
            xhttp.setRequestHeader("content-type", contentType);
        }
        if (authToken && authToken !== null) {
            xhttp.setRequestHeader("Authorization", "Bearer " + authToken);
        }
        xhttp.withCredentials = true;
        if (m !== "get") {
            xhttp.send(qsp);
        } else {
            xhttp.send();
        }
    };
}

export default APIService

