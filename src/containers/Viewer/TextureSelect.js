import React, { Component } from 'react'
import Select from '@material-ui/core/Select'
import MenuItem from '@material-ui/core/MenuItem'
import FormControl from '@material-ui/core/FormControl'
import InputLabel from '@material-ui/core/InputLabel'
import Viewer from './Viewer'


class TextureSelect extends Component {
    constructor() {
        super()
        this.state = {
            currentTexture: "None"
        }
    }

    handleChange(e) {
        this.setState({ currentTexture: e.target.value })
    }

    render() {
        return (
            <div >
                <FormControl style={{ position: "absolute", left: "10px", top: "10px" }}>
                    <InputLabel>Texture</InputLabel>
                    <Select
                        value={this.state.currentTexture}
                        onChange={(e) => this.handleChange(e)}
                    >
                        <MenuItem value="ceramic.jpeg">Texture1</MenuItem>
                        <MenuItem value="UV_Grid_Sm.jpg">Texture2</MenuItem>
                        <MenuItem value="copper.jpeg">Texture3</MenuItem>
                        {/* <MenuItem value="screw_texture4">Texture4</MenuItem> */}
                    </Select>
                </FormControl>
                <Viewer texture={this.state.currentTexture} />
            </div>
        )
    }
}

export default TextureSelect