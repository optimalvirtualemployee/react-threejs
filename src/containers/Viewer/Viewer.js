import React, { Component } from 'react'
import * as THREE from 'three'
import { OrbitControls } from '../../libs/OrbitControls'
import { STLLoader } from '../../libs/STLLoader'
const path = require('path')

let scene, sceneObject, renderer, camera, controls


class Viewer extends Component {

    componentDidMount() {
        scene = new THREE.Scene()

        // this.addBox()

        this.loadModel()

        this.initializeCamera()

        this.addLight()

        this.initializeRenderer()

        this.initializeOrbitControls()

        window.addEventListener('resize', this.onWindowResize, false)

        let animate = function () {
            requestAnimationFrame(animate)
            renderer.render(scene, camera)
            controls.update()
        }

        animate()
    }

    applyTexture(name) {
        const p = path.join(__dirname, `src/assets/${name}`)
        const texture = new THREE.TextureLoader().load(p)   //'https://images.unsplash.com/photo-1547056961-3c25e9140b05?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=1350&q=80'

        scene.children[1].material.map = texture;
        scene.children[1].material.needsUpdate = true

        // this.createExtrudeGeo()
    }

    addUV(geometry) {

        geometry.computeBoundingBox();

        var max = geometry.boundingBox.max, min = geometry.boundingBox.min;
        var offset = new THREE.Vector2(0 - min.x, 0 - min.y);
        var range = new THREE.Vector2(max.x - min.x, max.y - min.y);
        var faces = geometry.faces;

        geometry.faceVertexUvs[0] = [];

        for (var i = 0; i < faces.length; i++) {

            var v1 = geometry.vertices[faces[i].a],
                v2 = geometry.vertices[faces[i].b],
                v3 = geometry.vertices[faces[i].c];

            geometry.faceVertexUvs[0].push([
                new THREE.Vector2((v1.x + offset.x) / range.x, (v1.y + offset.y) / range.y),
                new THREE.Vector2((v2.x + offset.x) / range.x, (v2.y + offset.y) / range.y),
                new THREE.Vector2((v3.x + offset.x) / range.x, (v3.y + offset.y) / range.y)
            ]);
        }

        geometry.uvsNeedUpdate = true;
    }

    createExtrudeGeo() {
        let pointList = [
            [0, 0, 0],
            [0, 1000, 0],
            [750, 1000, 0],
            [750, 750, 0],
            [1000, 750, 0],
            [1000, 0, 0]
        ]

        let myPoints = [];

        for (let i = 0; i < pointList.length; i++) {

            let point = pointList[i];

            let x = point[0];
            let y = point[1];

            myPoints.push(new THREE.Vector2(x, y));
        }

        let myShape = new THREE.Shape(myPoints);
        let extrusionSettings = {
            amount: 2
        };

        let myGeometry = new THREE.ExtrudeGeometry(myShape, extrusionSettings);
    }

    componentDidUpdate() {
        if (this.props.texture !== "None") {
            this.applyTexture(this.props.texture)
        }
    }

    addBox() {
        const geo = new THREE.BoxGeometry()
        const mat = new THREE.MeshBasicMaterial({ color: 0x000000 })
        const cube = new THREE.Mesh(geo, mat)
        scene.add(cube)
    }

    addLight() {
        // var spotLight = new THREE.SpotLight(0xffffff);

        // scene.add(spotLight)

        // spotLight.position.set(100, 100, 100);

        // spotLight.parent = camera

        scene.add(new THREE.AmbientLight(0xffffff, 1))
    }

    loadModel() {
        const loader = new STLLoader();
        loader.load('./models/stl/vis_M8X15.STL', (bufferGeometry) => {
            const material = new THREE.MeshPhongMaterial({ color: 0xeeeeee });
            const geometry = new THREE.Geometry().fromBufferGeometry(bufferGeometry)
            sceneObject = new THREE.Mesh(geometry, material);

            scene.add(sceneObject);

            this.addUV(geometry)

            let cc = new THREE.Box3().setFromObject(sceneObject).getCenter();
            controls.target.set(cc.x, cc.y, cc.z);
        })
    }

    initializeCamera() {
        camera = new THREE.PerspectiveCamera(45, window.innerWidth / window.innerHeight, 0.1, 10000)
        camera.name = "Camera"
        camera.position.set(50, 50, 50)
    }

    initializeRenderer() {

        renderer = new THREE.WebGLRenderer()

        renderer.setClearColor(0xffffff, 1.0)

        renderer.setSize(window.innerWidth, window.innerHeight)

        var viewerContainer = document.getElementById("viewerContainer")

        viewerContainer.appendChild(renderer.domElement)

    }

    initializeOrbitControls() {
        controls = new OrbitControls(camera, renderer.domElement)
    }

    onWindowResize() {
        camera.aspect = window.innerWidth / window.innerHeight;
        camera.updateProjectionMatrix();
        renderer.setSize(window.innerWidth, window.innerHeight);
    }

    render() {
        return (
            <div id="viewerContainer" />
        )
    }

}

export default Viewer