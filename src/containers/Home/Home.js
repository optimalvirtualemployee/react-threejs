import React, { Component } from 'react'
import APIService from '../../api-service/APIService'
import MainSlider from '../../components/MainSlider/MainSlider'
import Service from '../../components/Service/Service'
import SubSlider from '../../components/SubSlider/SubSlider'
import About from '../../components/About/About'
import Subscribe from '../../components/Subscribe/Subscribe'
import Partner from '../../components/Partner/Partner'


class Home extends Component {
    constructor() {
        super()

        this.state = {

            mainSliderData: [
                {
                    head: "Lorem Ipsum is dummy Text",
                    paragraph: "Extremely intellectual &amp; challenging projects",
                    buttonText: "About Us"
                },
                {
                    head: "Extremely intellectual & challenging projects",
                    paragraph: "Lorem ipsum dolor sit amet consectetur adipisicing elit.",
                    buttonText: "Our Services"
                },
                {
                    head: "Lorem Ipsum is dummy Text",
                    paragraph: "Lorem ipsum dolor sit amet consectetur adipisicing elit.",
                    buttonText: "Contact Us"
                }
            ],

            // footerData: {
            //     head: "Dwight",
            //     paragraph: "Lorem ipsum dolor sit amet consectetur adipisicing elit. Iste mollitia deserunt repudiandae, ut minusassumenda architecto illum eaqueodio temporibus."
            // },

            serviceData: {
                head: "Services We Provide",
                paragraph: "Lorem ipsum dolor sit amet consectetur adipisicing elit. Corrupti sit labore doloresaliquam possimus deleniti cupiditate temporibus, repudiandae suscipit asperiores vel.",
                services: [
                    {
                        head: "Service 1",
                        paragraph: "Lorem ipsum dolor sit amet consectetur adipisicing elit. Id, minima. It is a long established factthat a reader will bedistracted by the readable.",
                        icon: "fa-industry"
                    },
                    {
                        head: "Service 2",
                        paragraph: "Lorem ipsum dolor sit amet consectetur adipisicing elit. Id, minima. It is a long established factthat a reader will bedistracted by the readable.",
                        icon: "fa-power-off"
                    },
                    {
                        head: "Service 3",
                        paragraph: "Lorem ipsum dolor sit amet consectetur adipisicing elit. Id, minima. It is a long established factthat a reader will bedistracted by the readable.",
                        icon: "fa-map-o"
                    },
                ]
            },

            aboutData: {
                head: "Who We Are",
                paragraph: "Lorem ipsum dolor sit amet consectetur adipisicing elit. Corrupti sit labore doloresaliquam possimus deleniti cupiditatetemporibus, repudiandae suscipit asperiores vel.",
                subHeading: "Dream BigInspire the World",
                subParagraph: "Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quam id quisquam ipsam molestiae ad eiusaccusantium? Nulla dolorem perferendis inventore!posuere cubilia Curae; Nunc non risus in justo convallis feugiat.",
                bulletPoints: [
                    "Products &amp; Solutions",
                    "Dream big inspiring solutions",
                    "Assured plant availability and operational security",
                    "Improved operating conditions",
                    "Relief of company's own maintenance resources"
                ]
            },

            partnerData: {
                head: "Over 12k+ Companies trust us",
                paragraph: "Lorem ipsum dolor sit amet consectetur adipisicing elit. Corrupti sit labore doloresaliquam possimus deleniti cupiditate temporibus, repudiandae suscipit asperiores vel.",
                images: [
                    "src/assets/images/logo1.jpg",
                    "src/assets/images/logo2.jpg",
                    "src/assets/images/logo3.jpg",
                    "src/assets/images/logo4.jpg",
                ]
            },

            subscribeData: {
                head: "Keep up to date - Get Email updates",
                paragraph: "Stay tuned for the latest company news."
            },

            subsliderData: [
                {
                    head: "Heading goes here",
                    paragraph: "Lorem ipsum dolor sit amet consectetur adipisicing elit. Temporibus voluptatum,nostrum sed alias accusamus earum.",
                    img: "src/assets/images/img4.jpg",
                    buttonText: "About Us"
                },
                {
                    head: "Heading goes here",
                    paragraph: "Lorem ipsum dolor sit amet consectetur adipisicing elit. Temporibus voluptatum,nostrum sed alias accusamus earum.",
                    img: "src/assets/images/img2.jpg",
                    buttonText: "Our Services"
                },
                {
                    head: "Heading goes here",
                    paragraph: "Lorem ipsum dolor sit amet consectetur adipisicing elit. Temporibus voluptatum,nostrum sed alias accusamus earum.",
                    img: "src/assets/images/img8.jpg",
                    buttonText: "Contact Us"
                }
            ]
        }
    }

    componentDidMount() {
        let service = new APIService()
        let url = "";
        let getResponse = function (response) {

        }


        service.ajaxRequest("get", url, null, getResponse)
    }

    render() {
        return (
            <div>
                <MainSlider slidesData={this.state.mainSliderData} />
                <Service serviceData={this.state.serviceData} />
                <SubSlider subsliderData={this.state.subsliderData} />
                <About aboutData={this.state.aboutData} />
                <Subscribe subscribeData={this.state.subscribeData} />
                <Partner partnerData={this.state.partnerData} />
            </div>
        )
    }
}

export default Home