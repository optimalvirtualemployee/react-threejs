import React from 'react';
import { Route, Switch, Redirect } from 'react-router-dom';

import Header from './components/Header/Header'
import NavBar from './components/NavBar/NavBar';
import Home from './containers/Home/Home'
import FabricationService from './containers/FabricationService/FabricationService'
import Product from './containers/Products/Product'
import DesignService from './containers/DesignService/DesignService'
import Contact from './containers/Contact/Contact'
import Footer from './components/Footer/Footer'
import Login from './components/Login/Login'
import Registration from './components/Registration/Registration'

export const Routes = () => {
    return (
        <div>
            <Header />
            <NavBar />
            <Switch>
                <Route exact path="/Home" component={Home} />
                <Route exact path="/">
                    <Redirect to="/Home" />
                </Route>
                <Route exact path="/FabricationService" component={FabricationService} />
                <Route exact path="/Product" component={Product} />
                <Route exact path="/DesignService" component={DesignService} />
                <Route exact path="/Contact" component={Contact} />
                <Route exact path="/Login" component={Login} />
                <Route exact path="/Registration" component={Registration} />
            </Switch>
            <Footer />
        </div>
    );
};