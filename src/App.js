import React, { Component } from 'react'
import { Router, Route, Link, browserHistory, IndexRoute } from 'react-router'
import Home from './containers/Home/Home'

class App extends Component {
    render() {
        return (
            <div>
                <Home />
            </div>
        )
    }
}

export default App;